# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | Y         |


---

### How to use 

## Basic Components
1. Basic control tools
    * **Brush**： 點擊右方工具欄的![](https://i.imgur.com/kQmNft9.png)。一開始初始的工具已經設為brush，當滑鼠移動時便會去隨時更新他的顏色以及粗細。

    * **Eraser**：點擊右方工具欄的![](https://i.imgur.com/XmfCVML.png)。功能是真實的將顏色從畫布上擦拭掉，而不是利用背景的顏色將他覆蓋。

    * **Color selector**：點擊右方工具欄選取想要的顏色，在畫圖、其他圖形或是輸入文字時會改變成所選的顏色。
    * **Brush size**：初始值為5px，點擊右方的range調整成想要的線條粗細。
2. Text input 
    * 先在右邊的text bar中輸入文字，接著點擊 ![](https://i.imgur.com/Snvzlw7.png)，當滑鼠移到畫布上時，文字一直顯示在滑鼠上，在要放上的地方再次點擊滑鼠，文字就會顯示在畫布上。

    * **font menu** (typeface and size)：可以在旁邊選擇文字的種類，直接調整畫筆粗細的range來改變文字的大小。
    
3. Cursor icon
    * 除了text、redo/undo、refresh、upload跟download以外，其他button按下去之後都會改變滑鼠的icon。
4. Refresh button
    * 點擊右方的 ![](https://i.imgur.com/jfclsGE.png)。將整個畫布清除，但是undo裡所儲存的紀錄不會被清空，也就是當按下refresh後再按下undo是可以將畫布回到清除前的。

## Advanced tools
1. Different brush shapes
    * 點擊右方工具欄的  ![](https://i.imgur.com/lg38mFu.png)、![](https://i.imgur.com/QvC3bIp.png)、![](https://i.imgur.com/sWrsoF2.png)。
    * **Circle**、**Triangle**、**Rectangle**：點擊後移動滑鼠來調整到想要的大小，在移動時圖形就會顯示出來，而不是等到放開後才顯示。
    * 每種形狀皆有實心與空心的圖形。
2. Un/Re-do button
    * **Undo**：點擊 ![](https://i.imgur.com/v5teAUT.png)回到上一步。
    * **Redo**：點擊 ![](https://i.imgur.com/GOIYBus.png)回復下一步。


3. Image tool
    * 點擊右方的"選擇檔案"，將想要的圖片upload上畫布。
    * 在滑鼠還未放開時都可以自由地調整圖片的大小。
4. Download
    * 點擊右方的 ![](https://i.imgur.com/tKxjT7D.png)，將現在的畫布下載並輸出成output.png。

## Function description

**Other useful widgets**
1. **Rainbow pen**：點擊右方的![](https://i.imgur.com/qoKTC8g.png)，此時滑鼠會變成![](https://i.imgur.com/4EtsAhZ.png)，功能類似brush，畫在畫布上的顏色會隨著滑鼠移動而改變成類似彩虹的顏色。
 
2. **Ctrl key**：當要upload圖片時，如果按住ctil調整圖片，他會依照原圖片圖檔的比例改變大小，直到放開ctrl後才能依照自己要的比例去調整。

3. **Fill**：點擊右方的 ![](https://i.imgur.com/8cDpuJy.png)，他會依照線在選的顏色將整個畫布填滿，由於不是直接換背景顏色，所以還是可以使用eraser擦拭掉。

4. **Music**：點擊右上方的音樂播放器可以播放背景音樂，音樂播完時會自己重播。

 
### Gitlab page link
https://s107062324.gitlab.io/AS_01_WebCanvas


<style>
table th{
    width: 100%;
}
</style>