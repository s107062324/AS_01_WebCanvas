var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");

// Initialize
var mounseDown = false;
var currentTool = "brush";
document.getElementsByTagName("body")[0].style.cursor = "url(img/mbrush.png), auto"; // mouse icon
var linewidth = 5; // the line and text size
var Style; // color
var Font = "Arial";
var undo_buf = [];
var redo_buf = [];
var lastPoint = {x: undefined, y: undefined};
var uploaded_img = new Image(); // Store the uploaded image
var pasting = false;
var drainbow = false;
var hue = 0;

var eraser = document.getElementById('eraser');
var range = document.getElementById("range");
var save = document.getElementById("save")
var myColor = document.getElementById("myColor");
var undo = document.getElementById("undo");
var redo = document.getElementById("redo");
var upload = document.getElementById("upload");
var text = document.getElementById("text");
var text_input = document.getElementById("text_input");
var font_selector = document.getElementById("font_selector");
var rainbow = document.getElementById("rainbow");
var line = document.getElementById("line");
var fillcanvas = document.getElementById("fill");

function ReloadImage(){
    var tmp = undo_buf.pop();
    ctx.putImageData(tmp, 0, 0);
    undo_buf.push(tmp);
}

canvas.onmousedown = function(e){
    var tmp = ctx.getImageData(0, 0, canvas.width, canvas.height);
    undo_buf.push(tmp);
    redo_buf = [];
    mounseDown = true;
    var x = e.offsetX;
    var y = e.offsetY;
    lastPoint = {"x": x, "y": y};
    ctx.save();
    var Text = text_input.value;
    if(currentTool == "text"){
        ctx.globalCompositeOperation = "source-over";
        var Font = document.getElementById("font_selector"); 
        ctx.font = (linewidth)+"px " + Font;
        ctx.fillStyle = document.getElementById("myColor").value;
        ctx.fillText(Text, x, y);
    }
};

canvas.onmousemove = function(e){
    var x = e.offsetX;
    var y = e.offsetY;
    Style = document.getElementById("myColor").value;
    
    if(currentTool == "text" && !mounseDown){
        ctx.globalCompositeOperation = "source-over";
        ctx.beginPath();
        ReloadImage();
        var Text = text_input.value;
        ctx.font = (linewidth*3)+"px " + Font;
        ctx.fillStyle = Style;
        ctx.fillText(Text, x, y);
    }
    if(mounseDown){
        ctx.save();
        ctx.strokeStyle = Style;
        ctx.lineWidth = linewidth;
        ctx.globalCompositeOperation = "source-over";
        ctx.lineCap = "round";
        ctx.lineJoin = "round";
        ctx.beginPath();
        if(pasting){
            ReloadImage();
            if(e.ctrlKey){
                // Ctrl key is pressed, keep ratio locked
                var scale_x = (x-lastPoint.x)/uploaded_img.width;
                var scale_y = (y-lastPoint.y)/uploaded_img.height;
                var max_scale = Math.max(scale_x, scale_y);
                ctx.drawImage(uploaded_img, lastPoint.x, lastPoint.y, uploaded_img.width*max_scale, uploaded_img.height*max_scale);
            }
            else{
                // Ctrl key isn't pressed, free ratio
                ctx.drawImage(uploaded_img, lastPoint.x, lastPoint.y, x - lastPoint.x, y - lastPoint.y);
            }
        }
        else if(currentTool == "eraser"){
            ctx.globalCompositeOperation = "destination-out";
            ctx.moveTo(lastPoint.x, lastPoint.y);
            ctx.lineTo(x, y);
            lastPoint = {x,y};
        }
        else if(currentTool == "dline"){
            ReloadImage();
            ctx.moveTo(lastPoint.x, lastPoint.y);
            ctx.lineTo(x, y);
        }
        else if(currentTool == "dcircle"){
            ReloadImage();
            var dx = x-lastPoint.x;
            var dy = y-lastPoint.y;
            var r = Math.sqrt(dx*dx + dy*dy)/2;
            ctx.arc(lastPoint.x+dx/2, lastPoint.y+dy/2, r, 0, Math.PI*2);
        }
        else if(currentTool == "drectangle"){
            ReloadImage();
            ctx.lineCap = "";
            ctx.lineJoin = "";
            ctx.rect(lastPoint.x, lastPoint.y, x-lastPoint.x, y-lastPoint.y);
        }
        else if(currentTool == "dtriangle"){
            ReloadImage();
            ctx.lineCap = "";
            ctx.lineJoin = "";
            ctx.moveTo((x+lastPoint.x)/2, lastPoint.y); //畫三條線
            ctx.lineTo(x, y);
            ctx.lineTo(lastPoint.x, y);
            ctx.lineTo((x+lastPoint.x)/2, lastPoint.y);
        }
        else if(currentTool == "dfillcir"){
            ReloadImage();
            var dx = x-lastPoint.x;
            var dy = y-lastPoint.y;
            var r = Math.sqrt(dx*dx + dy*dy)/2;
            ctx.arc(lastPoint.x+dx/2, lastPoint.y+dy/2, r, 0, Math.PI*2);
            ctx.fillStyle = ctx.strokeStyle; //實心要加的
            ctx.fill();
        }
        else if(currentTool == "dfillrec"){
            ReloadImage();
            ctx.lineCap = "";
            ctx.lineJoin = "";
            ctx.rect(lastPoint.x, lastPoint.y, x-lastPoint.x, y-lastPoint.y);
            ctx.fillStyle = ctx.strokeStyle; //實心要加的
            ctx.fill();
        }
        else if(currentTool == "dfilltri"){
            ReloadImage();
            ctx.lineCap = "";
            ctx.lineJoin = "";
            ctx.moveTo((x+lastPoint.x)/2, lastPoint.y); //畫三條線
            ctx.lineTo(x, y);
            ctx.lineTo(lastPoint.x, y);
            ctx.lineTo((x+lastPoint.x)/2, lastPoint.y);
            ctx.fillStyle = ctx.strokeStyle; //實心要加的
            ctx.fill();
        }
        else if(currentTool == "brush"){
            if(drainbow){
                ctx.strokeStyle = `hsl(${ hue }, 90%, 50%)`; // change to rainbow color
                if (hue >= 360) hue = 0;
                hue += 7;
            }
            ctx.moveTo(lastPoint.x, lastPoint.y);
            ctx.lineTo(x, y);
            lastPoint = {x,y};
        }
        ctx.stroke();
    }
};

canvas.onmouseup = function(){
    mounseDown = false;
    if(pasting){
        document.getElementsByTagName("body")[0].style.cursor = "auto";
        pasting = false;
    }
    else if(currentTool == "text"){
        currentTool = "";
    }
    ctx.closePath();
}

var linewidth_update = function () {
    var thicknessRange = $("#range");
    thicknessRange.next().html(linewidth + " px");
};

brush.onclick = function(){
    currentTool = "brush";
    drainbow = false;
    document.getElementsByTagName("body")[0].style.cursor = "url(img/mbrush.png), auto";
}

eraser.onclick = function(){
    currentTool = "eraser";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/meraser.png), auto";
}

save.onclick = function(){
    let saveA = document.createElement('a');
    let imgUrl = canvas.toDataURL('image/png');
    saveA.href = imgUrl;
    saveA.download = "output.png";
    saveA.click();
}

range.onchange = function(){
    linewidth = this.value;
    linewidth_update();
}

line.onclick = function(){
    currentTool = "dline";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/dline.png), auto";
}

circle.onclick = function(){
    currentTool = "dcircle";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/mcircle.png), auto";
}

rectangle.onclick = function(){
    currentTool = "drectangle";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/mrectangle.png), auto";
}

triangle.onclick = function(){
    currentTool = "dtriangle";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/mtriangle.png), auto";
}

fillcir.onclick = function(){
    currentTool = "dfillcir";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/mcirclefill.png), auto";
}

fillrec.onclick = function(){
    currentTool = "dfillrec";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/mrectanglefill.png), auto";
}

filltri.onclick = function(){
    currentTool = "dfilltri";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/mtrianglefill.png), auto";
}

undo.onclick = function(){
    if(undo_buf.length > 0){
        redo_buf.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        ctx.putImageData(undo_buf.pop(), 0, 0);
    }
}

redo.onclick = function(){
    if(redo_buf.length > 0){
        undo_buf.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
        ctx.putImageData(redo_buf.pop(), 0, 0);
    }
}

clean.onclick = function(){
    undo_buf.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    redo_buf.length = 0;
    redo_buf.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
}

upload.onchange = function(event1){
    document.getElementsByTagName("body")[0].style.cursor = "auto";
    var tmp = ctx.getImageData(0, 0, canvas.width, canvas.height);
    undo_buf.push(tmp);
    redo_buf = [];
    pasting = true;
    
    var reader = new FileReader();
    try{
        reader.readAsDataURL(event1.target.files[0]);
        reader.onload = function(event2){
            uploaded_img.src = event2.target.result;
        };
    } catch(exception){
        console.log(exception);
    }
    currentTool = "";
}

text.onclick = function(){
    var tmp = ctx.getImageData(0, 0, canvas.width, canvas.height);
    undo_buf.push(tmp);
    redo_buf = [];
    document.getElementsByTagName("body")[0].style.cursor = "auto";
    currentTool = "text";
    ctx.font = (linewidth)+"px " + Font;
    ctx.fillStyle = document.getElementById("myColor").value;
}

font_selector.onchange = function(e){
    Font = this.options[this.selectedIndex].value;
}

rainbow.onclick = function(){
    drainbow = true;
    currentTool = "brush";
    document.getElementsByTagName("body")[0].style.cursor = "url(img/rainbowpen.png), auto";
}

fillcanvas.onclick = function(){
    document.getElementsByTagName("body")[0].style.cursor = "auto";
    ctx.globalCompositeOperation = "source-over";
    currentTool = "";
    ctx.fillStyle = Style;
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}
